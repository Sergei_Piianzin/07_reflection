package com.epam.reflection.method;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class MethodsPrinter {
  private Class<?> clazz;

  public static MethodsPrinter getInstance(String className) {
    MethodsPrinter instance = new MethodsPrinter();
    try {
      instance.clazz = Class.forName(className);
    } catch (ClassNotFoundException e) {
      return null;
    }
    return instance;
  }

  private MethodsPrinter() {}

  public void printMethods() {
    Method[] methods = clazz.getDeclaredMethods();
    for (Method m : methods) {
      System.out.printf("%nMethod: %s%n", m.getName());
      printAnnotations(m);
    }
  }

  private void printAnnotations(Method m) {
    Annotation[] annotations = m.getAnnotations();
    if (annotations.length == 0) {
      System.out.println("There is no annotations");
    } else {
      System.out.println("Annotations:");
      for (Annotation a : annotations) {
        String nameAndParameters = a.toString();
        System.out.printf("%s%n", a.toString());
      }
    }
  }

}
