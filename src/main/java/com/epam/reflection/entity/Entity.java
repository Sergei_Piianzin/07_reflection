package com.epam.reflection.entity;

import java.util.Formatter;

import com.epam.reflection.annotation.Secured;

public class Entity {
  String title;
  String url;
  int maxClients;
  int nodesCount;

  public Entity() {
    title = "empty";
    url = "empty";
    maxClients = 0;
    nodesCount = 0;
  }

  public Entity(String title, String url, int maxClients, int nodesCount) {
    this.title = title;
    this.url = url;
    this.maxClients = maxClients;
    this.nodesCount = nodesCount;
  }

  @Secured(score = 95, title = "strong")
  public String getPage(String name) {
    return createResponse(name);
  }

  private String createResponse(String name) {
    String template = "<!DOCTYPE html><html><head></head><body>Hello, %s!</body></html>";
    try (Formatter formatter = new Formatter()) {
      formatter.format(template, name);
      return formatter.toString();
    }
  }

  @Secured(score = 40)
  public boolean isAvailable(int maxClientsOnNode) {
    int currentClientsOnNode = maxClients / nodesCount;
    return currentClientsOnNode < maxClientsOnNode;
  }

  public String getTitle() {
    return title;
  }

  public String getURL() {
    return url;
  }

  public int getMaxClients() {
    return maxClients;
  }

  public int getNodesCount() {
    return nodesCount;
  }


}
