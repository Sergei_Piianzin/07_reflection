package com.epam.reflection.app;

import com.epam.reflection.method.MethodsPrinter;

public class App {
  public static void main(String[] args) {
    MethodsPrinter methodsPrinter = MethodsPrinter.getInstance("com.epam.reflection.entity.Entity");
    methodsPrinter.printMethods();
  }
}
