package com.epam.reflection_test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.epam.reflection.entity.Entity;

public class EntityTest {

  Entity entityTest;

  @Before
  public void init() {
    entityTest = new Entity("Test", "http://test.com", 1000, 2);
  }
  
  @Test
  public void shouldCreateEntityWithDefaultFields() {
    Entity innerEntity = new Entity();
    assertThat(innerEntity.getTitle(), is("empty"));
    assertThat(innerEntity.getURL(), is("empty"));
    assertThat(innerEntity.getMaxClients(), is(0));
    assertThat(innerEntity.getNodesCount(), is(0));
  }

  @Test
  public void shouldGetFilledTemplate() {
    String result = entityTest.getPage("Tim");
    assertThat(result, is("<!DOCTYPE html>\n<html><head></head><body>Hello, Tim!</body></html>"));
  }

  @Test
  public void shouldReturnTrue() {
    boolean result = entityTest.isAvailable(700);
    assertThat(result, is(Boolean.TRUE));

  }

  @After
  public void tearDown() {
    entityTest = null;
  }
}
