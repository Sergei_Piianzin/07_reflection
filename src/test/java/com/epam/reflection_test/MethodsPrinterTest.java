package com.epam.reflection_test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import com.epam.reflection.method.MethodsPrinter;

public class MethodsPrinterTest {

  @Test
  public void shouldReturnNullInUnknownClass() {
    MethodsPrinter printer = MethodsPrinter.getInstance("");
    assertNull(printer);
  }

  @Test
  public void shouldReturnInstanse() {
    MethodsPrinter printer = MethodsPrinter.getInstance("java.lang.Object");
    assertNotNull(printer);
  }

}
